var express = require('express');
var app = express();
var applyRoutes= express.Router();


// Require Item model in our routes module
var Apply = require('../models/Apply');

applyRoutes.route('/add').post(function (req, res) {
    var apply = new Apply(req.body);
    apply.save()
      .then(item => {
      res.status(200).json({'apply': 'Apply added successfully'});
      })
      .catch(err => {
      res.status(400).send("unable to save to database");
      });
  });


  applyRoutes.route('/').get(function (req, res) {
    Apply.find(function (err, applys){
    if(err){
      console.log(err);
    }
    else {
      res.json(applys);
    }
  });
  });

  applyRoutes.route('/delete/:id').get(function (req, res) {
    Apply.findByIdAndRemove({_id: req.params.id}, function(err, apply){
        if(err) res.json(err);
        else res.json('Successfully removed');
    });
  });
  

  module.exports =applyRoutes;
  