var express = require('express');
var app = express();
var recruitRoutes = express.Router();

// Require Item model in our routes module
var Recruit = require('../models/Recruit');

// Defined store route
recruitRoutes.route('/add').post(function (req, res) {
  var recruit = new Recruit(req.body);
  recruit.save()
    .then(item => {
    res.status(200).json({'recruit': 'Recruit added successfully'});
    })
    .catch(err => {
    res.status(400).send("unable to save to database");
    });
});


// Defined get data(index or listing) route

recruitRoutes.route('/').get(function (req, res) {
  Recruit.find(function (err, recruits){
  if(err){
    console.log(err);
  }
  else {
    res.json(recruits);
  }
});
});



// Defined edit route
recruitRoutes.route('/edit/:id').get(function (req, res) {

  var id = req.params.id;
  Recruit.findById(id, function (err, recruit){
      res.json(recruit);
  });
});


//  Defined update route
recruitRoutes.route('/update/:id').post(function (req, res) {
    Recruit.findById(req.params.id, function(err, recruit) {
    if (!recruit)
      return next(new Error('Could not load Document'));
    else {
        recruit.name = req.body.name;
        recruit.empID = req.body.empID;
        recruit.role = req.body.role;
        recruit.gmail = req.body.gmail;
        recruit.password = req.body.password;


        recruit.save().then(recruit => {
          res.json('Update complete');
      })
      .catch(err => {
            res.status(400).send("unable to update the database");
      });
    }
  });
});

// Defined delete | remove | destroy route
recruitRoutes.route('/delete/:id').get(function (req, res) {
    Recruit.findByIdAndRemove({_id: req.params.id}, function(err, recruit){
        if(err) res.json(err);
        else res.json('Successfully removed');
    });
});


module.exports =recruitRoutes;

