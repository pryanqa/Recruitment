
const express = require('express'),
      path = require('path'),
      bodyParser = require('body-parser'),
      cors = require('cors'),
      mongoose = require('mongoose'),
      config = require('./config/DB');
      recruitRoutes = require('./expressRoutes/recruitRoutes');
      applyRoutes = require('./expressRoutes/applyRoutes');
      postRoutes = require('./expressRoutes/postRoutes');
      
      mongoose.Promise = global.Promise;
      mongoose.connect(config.DB).then(
          () => {console.log('Database is connected') },
          err => { console.log('Can not connect to the database'+ err)}
        );

      const app = express();
      app.use(bodyParser.json());
      app.use(cors());
      const port = process.env.PORT || 4000;
      app.use('/recruits', recruitRoutes);
      app.use('/applys', applyRoutes);
      app.use('/posts', postRoutes);
 
       const server = app.listen(port, function(){
         console.log('Listening on port ' + port);
       });
































































// var express = require("express");
// var app = express();
// var port = 3000;
 
// var mongoose = require("mongoose");
// mongoose.Promise = global.Promise;
// mongoose.connect("mongodb://localhost:27017/employees");

// app.use("/", (req, res) => {
//     res.sendFile(__dirname + "/src/app/app.component.html");
//     // res.send("Hello World");
//   });
 
// app.listen(port, () => {
//   console.log("Server listening on port " + port);
// });