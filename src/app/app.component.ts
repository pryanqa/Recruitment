import { Component } from '@angular/core';
import { Router } from '../../node_modules/@angular/router';
import { RecruitService } from './recruit.service';
import { Subject } from '../../node_modules/rxjs';
import { LoginService } from './auth/login.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'recruitment';
  users : any;
 
  constructor(private router:Router,
    private service: RecruitService,
    private logins : LoginService
   ){}
  }


