import { Component, OnInit } from '@angular/core';
import { RecruitService } from '../../../recruit.service';
import { HttpClient } from '../../../../../node_modules/@angular/common/http';
import { Observable } from '../../../../../node_modules/rxjs';



@Component({
  selector: 'app-shortlist',
  templateUrl: './shortlist.component.html',
  styleUrls: ['./shortlist.component.css']
})
export class ShortlistComponent implements OnInit {

  recruits :any;

  constructor(private http: HttpClient, private service: RecruitService) { }

  ngOnInit() {
    this.getRecruits();
  }
  getRecruits(){
    this.service.getRecruits().subscribe(res => {
      this.recruits =res;
    })
  }

  
deleteRecruit(id) {
  this.service.deleteRecruit(id).subscribe(res => {
    console.log('Deleted');
  });
}

}
