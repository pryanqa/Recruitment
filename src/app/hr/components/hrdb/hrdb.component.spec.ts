import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HrdbComponent } from './hrdb.component';

describe('HrdbComponent', () => {
  let component: HrdbComponent;
  let fixture: ComponentFixture<HrdbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HrdbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HrdbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
