import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RecruitService } from '../../../recruit.service';


@Component({
  selector: 'app-hrdb',
  templateUrl: './hrdb.component.html',
  styleUrls: ['./hrdb.component.css']
})
export class HrdbComponent implements OnInit {

  angForm: FormGroup;
  constructor(private service: RecruitService, private fb: FormBuilder, private router: Router) {
    this.createForm();
  }
    createForm() {
    this.angForm = this.fb.group({
      title: ['', Validators.required ],
      location: ['', Validators.required ],
      experience: ['', Validators.required ],
      type: ['', Validators.required ],
      skills: ['', Validators.required ],
      description: ['', Validators.required ]
   });

   }
   addPost(title, location,  experience, type, skills, description) {
    this.service.addPost(title, location,  experience, type, skills, description);
    console.log(title);
}
activeView="user";
setView (viewName){
  this.activeView = viewName
}


  ngOnInit() {
  }

}
