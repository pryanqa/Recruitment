import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators, ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RecruitService } from '../../../recruit.service';


@Component({
  selector: 'app-finalised',
  templateUrl: './finalised.component.html',
  styleUrls: ['./finalised.component.css']
})
export class FinalisedComponent implements OnInit {

  title = 'Add employee';
  angForm: FormGroup;
  constructor( private recruitservice: RecruitService ,private fb: FormBuilder ) {

    
    this.createForm();
   }
   createForm() {
    this.angForm = this.fb.group({
      name: ['', Validators.required ],
      empID: ['', Validators.required ],
      role: ['', Validators.required ],
      gmail: ['', Validators.required ],
      password: ['', Validators.required ]

   });
  }
  addRecruit(name, empID,role, gmail,password ) {
      this.recruitservice.addRecruit(name, empID,role, gmail,password);
  }


  ngOnInit() {
  }

}
