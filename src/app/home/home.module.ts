import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './components/home/home.component';
import { CarrersComponent } from './components/carrers/carrers.component';
import { FormsModule } from '../../../node_modules/@angular/forms';
import { AuthGuard } from '../auth.guard';
import { LoginService } from '../auth/login.service';
import { FileSelectDirective } from '../../../node_modules/ng2-file-upload';
import { HttpClientModule } from '../../../node_modules/@angular/common/http';
import { FormComponent } from './components/form/form.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    

  ],
  exports:[
    HomeComponent,
    CarrersComponent,
    FormComponent

  ],
  providers:[AuthGuard, LoginService ],
  declarations: [HomeComponent, FileSelectDirective, CarrersComponent, FormComponent]
})
export class HomeModule { }
