import { Component, OnInit } from '@angular/core';
import { Observable } from '../../../../../node_modules/rxjs';
import { RecruitService } from '../../../recruit.service';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  jobs:any;
  constructor(private service: RecruitService) { }

  ngOnInit() {
    this.getPosts();
  }
  getPosts(){
    this.service.getPosts().subscribe(res => {
      this.jobs =res;
    })
  }

  
 
}





















// showFile = false;
//   fileUploads: Observable<string[]>;
// showFiles(enable: boolean) {
//   this.showFile = enable;

//   if (enable) {
//     this.fileUploads = this.uploadService.getFiles();
//   }
// }