import { Injectable } from '@angular/core';
import { HttpClient, HttpEventType, HttpHeaders } from '@angular/common/http';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { Observable } from '../../node_modules/rxjs';
import{ map } from 'rxjs/operators';
import { HttpRequest } from '../../node_modules/@types/selenium-webdriver/http';
import { HttpEvent } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class RecruitService {

  result: any;
  constructor(private http: HttpClient) { }

  addPost(title,location, experience, skills, type ,description) {
   
    const uri = 'http://localhost:4000/posts/add';
    const obj = {
      title: title,
      location:location,
      experience:experience,
      skills:skills,
      type:type,
      description:description
    };
    console.log("iio")
    this.http.post(uri, obj)
        .subscribe(res => console.log('Done'));
  }

  addRecruit(name, empID,role, gmail,password) {
    const uri = 'http://localhost:4000/recruits/add';
    const obj = {
      name: name,
      empID:empID,
      role:role,
      gmail:gmail,
      password:password
    };
    this.http.post(uri, obj)
        .subscribe(res => console.log('Done'));
  }
  addCV(file){
    const uri='http://localhost:4000/files/upload';
    const obj={
      file:file
    }
  };
  
  
  addApply(name, qualifications,experience,
     skills,role,gmail, ref,password, round1,round2,selected) {
    const uri = 'http://localhost:4000/applys/add';
    const obj = {
      name: name,
      qualifications:qualifications,
      experience:experience,
      skills:skills,
      role:role,
      gmail: gmail,
      ref:ref,
      password:password,
      round1:round1,
      round2:round2,
      selected:selected
   
    };
    this.http.post(uri, obj)
        .subscribe(res => console.log('Done'));
  }
  getRecruits() {
    const uri = 'http://localhost:4000/recruits';
    return this
            .http
            .get(uri).pipe(
              map(res => {
                return res;
              })
            )
            
  }
  getPosts() {
    const uri = 'http://localhost:4000/posts';
    return this
            .http
            .get(uri).pipe(
              map(res => {
                return res;
              })
            )
            
  }
  getApplys() {
    const uri = 'http://localhost:4000/applys';
    return this
            .http
            .get(uri).pipe(
              map(res => {
                return res;
              })
            )
            
  }

  editRecruit(id) {
    const uri = 'http://localhost:4000/recruits/edit/' + id;
    return this
            .http
            .get(uri)
            .pipe(
              map(res => {
                return res;
              })
            )
}
editPost(id) {
  const uri = 'http://localhost:4000/posts/edit/' + id;
  return this
          .http
          .get(uri)
          .pipe(
            map(res => {
              return res;
            })
          )
}

updatePost(title,location, experience, skills, type ,description, id) {
  const uri = 'http://localhost:4000/posts/update/' + id;

  const obj = {
    
    title: title,
    location:location,
    experience:experience,
    skills:skills,
    type:type,
    description:description

  };
  this
    .http
    .post(uri, obj)
    .subscribe(res => console.log('Done'));
}


updateRecruit(name, empID, role,gmail,password, id) {
  const uri = 'http://localhost:4000/recruits/update/' + id;

  const obj = {
    name: name,
    empID: empID,
    role: role,
    gmail: gmail,
    password: password

  };
  this
    .http
    .post(uri, obj)
    .subscribe(res => console.log('Done'));
}


deleteRecruit(id) {
  const uri = 'http://localhost:4000/recruits/delete/' + id;
debugger
      return this
          .http
          .get(uri).pipe(
            map(res => {
              return res;
            })
          )
}

deletePost(id) {
  const uri = 'http://localhost:4000/posts/delete/' + id;

      return this
          .http
          .get(uri).pipe(
            map(res => {
              return res;
            })
          )
}

deleteApply(id) {
  const uri = 'http://localhost:4000/applys/delete/' + id;

      return this
          .http
          .get(uri).pipe(
            map(res => {
              return res;
            })
          )
}



}





























// pushFileToStorage(file: File) {
//   const formdata: FormData = new FormData();

//   formdata.append('file', file);
//   const url = '/post';
//   return this.http.post(url , formdata);
// }

// getFiles(): Observable<any> {
//   return this.http.get('/getallfiles');
// }