import { Injectable } from '@angular/core';
import { Router } from '../../../node_modules/@angular/router';
import { Subject } from '../../../node_modules/rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { RecruitService } from '../recruit.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  result: any;
  constructor(private router:Router, 
    private http: HttpClient, 
    private service: RecruitService) { }
 

sendToken(token: string) {
  sessionStorage.setItem("useremail", token)
}

getToken() {
  return sessionStorage.getItem("useremail")
}

isLoggedIn()
{
  return this.getToken() !== null;
}
logout()
{
  sessionStorage.clear();
  this.router.navigate(["home"]);
}
}








