var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var Apply = new Schema({
    name: String,
    qualifications :String,
    experience: String,
    skills: String,
    role:String,
    gmail:String,
    ref:String,
    password:String,
    round1: Number,
    round2: Number,
   
  
  },{
      collection: 'applys'
  });

  
module.exports = mongoose.model('Apply', Apply);
