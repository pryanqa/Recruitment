var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Post = new Schema({
    title: String,
    location :String,
    experience: String,
    skills: String,
    type:String,
    description:String,
  
  },{
      collection: 'posts'
  });


module.exports = mongoose.model('Post', Post);
